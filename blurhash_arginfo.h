/* This is a generated file, edit the .stub.php file instead.
 * Stub hash: fccf05f54982a8a06a862e4b392dc4fc4d3a46fd */

ZEND_BEGIN_ARG_WITH_RETURN_TYPE_INFO_EX(arginfo_bh_encode, 0, 3, IS_STRING, 0)
	ZEND_ARG_TYPE_INFO(0, xComponent, IS_LONG, 0)
	ZEND_ARG_TYPE_INFO(0, yComponent, IS_LONG, 0)
	ZEND_ARG_TYPE_INFO(0, file, IS_STRING, 0)
ZEND_END_ARG_INFO()

ZEND_FUNCTION(bh_encode);

static const zend_function_entry ext_functions[] = {
	ZEND_FE(bh_encode, arginfo_bh_encode)
	ZEND_FE_END
};

