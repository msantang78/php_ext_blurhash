--TEST--
Check if blurhash is loaded
--EXTENSIONS--
blurhash
--FILE--
<?php
echo 'The extension "blurhash" is available';
?>
--EXPECT--
The extension "blurhash" is available
