--TEST--
test2() Basic test
--EXTENSIONS--
blurhash
--FILE--
<?php
var_dump(bh_encode(4,3,'./tests/test.jpg'));
?>
--EXPECT--
string(28) "LWNmvQ?a_4oMo|Rja0t6-qoMIUNF"
